# AVS Device SDK Setup

## Getting Started on Ubuntu Linux


### SDK setup guide for C++


* [AVS Device SDK Setup on Ubuntu 17.10](avs-device-sdk-setup-ubuntu-17.10.md)
* [AVS Device SDK Setup on Ubuntu 17.04](avs-device-sdk-setup-ubuntu-17.04.md)


Note that Ubuntu 16.04 LTS or earlier releases may not be suitable 
for AVS Device SDK setup (for C++). 
They do not meet the SDK package requirements,
and it may be a bit of hassle to upgrade many dependent packages.

Typically, distros like Fedora or OpenSuse tend to have more up-to-date packages,
but I haven't tried setting up the AVS Device SDK on these distros.
