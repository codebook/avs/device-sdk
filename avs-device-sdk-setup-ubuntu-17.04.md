# Getting Started with AVS Device SDK on Ubuntu Linux

### Setting up AVS Device SDK on Ubuntu 17.04 (Incomplete)


_Note: I moved my dev setup to an Ubuntu 17.10 machine. This 17.04 doc is no longer maintained._

* [AVS Device SDK Setup on Ubuntu 17.10](avs-device-sdk-setup-ubuntu-17.10.md)



## Core Dependencies

### gcc/clang (Required: gcc 4.8.5 or clang 3.3)

    $ gcc --version
    gcc (Ubuntu 6.3.0-12ubuntu2) 6.3.0 20170406
    $ clang --version
    clang version 4.0.0-1ubuntu1 (tags/RELEASE_400/rc1)

### cmake (Required: cmake 3.1)

    $ cmake --version
    cmake version 3.7.2

### libcurl3 (Required: libcurl3 7.50.2)

    $ apt-cache policy libcurl3
    libcurl3:
      Installed: 7.52.1-4ubuntu1.1

### nghttp2 (Required: nghttp2 1.0)

    $ apt-cache policy nghttp2
    nghttp2:
      Installed: 1.19.0-2

### OpenSSL (Required: openssl 1.0.2)

    $ openssl version
    OpenSSL 1.0.2g  1 Mar 2016

### Doxygen (Required: doxygen 1.8.13)

    $ doxygen --version
    1.8.13

### SQLite 3 (Required: sqlite3 3.19.3)

    $ sqlite3 --version
    3.16.2 2017-01-06 16:32:41 a65a62893ca8319e89e48b8a38cf8a59c69a8209


All core dependencies are met by Ubuntu Zesty (17.04) packages except for sqlite3.

I just downloaded a new version from [the SQLite download page](https://www.sqlite.org/download.html),
and manually installed it. The prebuilit binary found on this site was 32 bit version,
and I had to install the necessary 32 bit library.

    sudo apt install -y libc6:i386

After this,

    $ ~/bin/sqlite3 --version
    3.20.1 2017-08-24 16:21:36 8d3a7ea6c5690d6b7c3767558f4f01b511c55463e3f9e64506801fe9b74dce34

and now all the core requirements are met.



## MediaPlayer Dependencies


    $ apt-cache policy libgstreamer1.0
    libgstreamer1.0-0:
      Installed: 1.10.4-1
    $ apt-cache policy gstreamer1.0-plugins-base
    gstreamer1.0-plugins-base:
      Installed: 1.10.4-1ubuntu1
    $ apt-cache policy gstreamer1.0-plugins-good
    gstreamer1.0-plugins-good:
      Installed: 1.10.4-1ubuntu1
    $ apt-cache policy gstreamer1.0-plugins-ugly
    gstreamer1.0-plugins-ugly:
      Installed: 1.10.4-1ubuntu1


Required versions are 1.8 or later, and hence all dependencies are met.



## Sample App Dependencies


    $ apt-cache policy portaudio19-dev
    portaudio19-dev:
      Installed: 19.6.0-1



## Music Provider Dependencies


    $ apt-cache policy gstreamer1.0-plugins-bad
    gstreamer1.0-plugins-bad:
      Installed: 1.10.4-1ubuntu1

    $ apt-cache policy libgcrypt20
    libgcrypt20:
      Installed: 1.7.6-1ubuntu0.2

    $ apt-cache policy libsoup2.4
    libsoup2.4-1:
      Installed: 2.56.0-2ubuntu0.1

    $ apt-cache policy libfaad-dev
    libfaad-dev:
      Installed: 2.8.0~cvs20161113-1



## Sensory Dependencies

https://github.com/Sensory/alexa-rpi



## Kitt.ai Dependencies

    apt-cache policy libatlas-base-dev
    libatlas-base-dev:
      Installed: 3.10.3-1ubuntu1


https://github.com/Kitt-AI/snowboy/



## Unit Test Framework

    $ apt-cache policy googletest
    googletest:
      Installed: 1.8.0-6

    $ apt-cache policy libgtest-dev
    libgtest-dev:
      Installed: 1.8.0-6




## Prerequisites

### AuthServer

    $ pip show flask
    Name: Flask
    Version: 0.12.2

    $ pip show requests
    Name: requests
    Version: 2.18.4


### Device Type ID, Client ID, and Client Secret

https://github.com/alexa/alexa-avs-sample-app/wiki/Create-Security-Profile



## Build



_tbd_



### Application settings


_tbd_




## Run


### Run AuthServer


### Run unit tests


### Run sample app



## Install


_tbd_






## References

* [AVS Device SDK](https://developer.amazon.com/alexa-voice-service/sdk)
* [alexa/avs-device-sdk](https://github.com/alexa/avs-device-sdk)
* [AlexaClientSDK Doc](https://alexa.github.io/avs-device-sdk/)
* [Linux Quick Start Guide](https://github.com/alexa/avs-device-sdk/wiki/Linux-Quick-Start-Guide)
* [Raspberry Pi Quick Start Guide](https://github.com/alexa/avs-device-sdk/wiki/Raspberry-Pi-Quick-Start-Guide)



