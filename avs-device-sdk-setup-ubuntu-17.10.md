# Getting Started with AVS Device SDK on Ubuntu Linux

### Setting up AVS Device SDK on Ubuntu 17.10


I'm following this doc, [Linux Quick Start Guide](https://github.com/alexa/avs-device-sdk/wiki/Linux-Quick-Start-Guide),
on Ubuntu 17.10 (beta, as of this writing)
and making notes.



## Core Dependencies

### gcc/clang (Required: gcc 4.8.5 or clang 3.3)

    $ sudo apt install -y build-essential

    $ gcc --version
    gcc (Ubuntu 7.2.0-8ubuntu2) 7.2.0

    $ clang --version
    clang version 4.0.1-6 (tags/RELEASE_401/final)

### cmake (Required: cmake 3.1)

    $ cmake --version
    cmake version 3.9.1

### libcurl3 (Required: libcurl3 7.50.2)

    $ apt-cache policy libcurl3
    libcurl3:
      Installed: 7.55.1-1ubuntu2

### nghttp2 (Required: nghttp2 1.0)

    $ sudo apt install nghttp2
    $ apt-cache policy nghttp2
    nghttp2:
      Installed: 1.25.0-1

### OpenSSL (Required: openssl 1.0.2)

    $ openssl version
    OpenSSL 1.0.2g  1 Mar 2016

### Doxygen (Required: doxygen 1.8.13)

    $ sudo apt install -y doxygen
    $ doxygen --version
    1.8.13

### SQLite 3 (Required: sqlite3 3.19.3)

    $ sudo apt install -y sqlite3
    $ sqlite3 --version
    3.19.3 2017-06-08 14:26:16 0ee482a1e0eae22e08edc8978c9733a96603d4509645f348ebf55b579e89636b


All core dependencies are met by Ubuntu Aardvark (17.10) packages.



## MediaPlayer Dependencies


    $ apt-cache policy libgstreamer1.0
    libgstreamer1.0-0:
      Installed: 1.12.3-1

    $ apt-cache policy gstreamer1.0-plugins-base
    gstreamer1.0-plugins-base:
      Installed: 1.12.3-1

    $ apt-cache policy gstreamer1.0-plugins-good
    gstreamer1.0-plugins-good:
      Installed: 1.12.3-1ubuntu1

    $ apt-cache policy gstreamer1.0-plugins-ugly
    gstreamer1.0-plugins-ugly:
      Installed: 1.12.3-1


Required versions are 1.10.4 or later, and hence all dependencies are met.



## Sample App Dependencies


    $ sudo apt install -y portaudio19-dev
    $ apt-cache policy portaudio19-dev
    portaudio19-dev:
      Installed: 19.6.0-1

(Required version is v190600_20161030, which I presume corresponds to 19.6.0.)


## Music Provider Dependencies


    $ apt-cache policy gstreamer1.0-plugins-bad
    gstreamer1.0-plugins-bad:
      Installed: 1.12.3-1ubuntu1

    $ apt-cache policy libgcrypt20
    libgcrypt20:
      Installed: 1.7.8-2ubuntu1

    $ apt-cache policy libsoup2.4
    libsoup2.4-1:
      Installed: 2.56.1-1

    $ sudo apt install -y libfaad-dev
    $ apt-cache policy libfaad-dev
    libfaad-dev:
      Installed: 2.8.1-2



## Sensory Dependencies

https://github.com/Sensory/alexa-rpi



## Kitt.ai Dependencies

    $ sudo apt install -y libatlas-base-dev
    $ apt-cache policy libatlas-base-dev
    libatlas-base-dev:
      Installed: 3.10.3-5


https://github.com/Kitt-AI/snowboy/



## Unit Test Framework

    $ sudo apt install -y googletest
    $ apt-cache policy googletest
    googletest:
      Installed: 1.8.0-6

    $ sudo apt install -y libgtest-dev
    $ apt-cache policy libgtest-dev
    libgtest-dev:
      Installed: 1.8.0-6




## Build/Run Prerequisites

### AuthServer

    $ sudo apt install -y python-pip
    $ pip --version
    pip 9.0.1 from /usr/lib/python2.7/dist-packages (python 2.7)

    $ pip install flask requests

    $ pip show flask
    Name: Flask
    Version: 0.12.2

    $ pip show requests
    Name: requests
    Version: 2.18.4


### Device Type ID, Client ID, and Client Secret


https://github.com/alexa/alexa-avs-sample-app/wiki/Create-Security-Profile

* Allowed Origins: http://localhost:3000
* Allowed Return URLs: http://localhost:3000/authresponse




## CMake


_tbd_



     cmake <path-to-source>  -DCMAKE_INSTALL_PREFIX=<path-to-out-of-source-build>


### Build with Sensory

    cmake <path-to-source> -DSENSORY_KEY_WORD_DETECTOR=ON -DSENSORY_KEY_WORD_DETECTOR_LIB_PATH=.../alexa-rpi/lib/libsnsr.a -DSENSORY_KEY_WORD_DETECTOR_INCLUDE_DIR=.../alexa-rpi/include


### Build with KITT.ai

    cmake <path-to-source> -DKITTAI_KEY_WORD_DETECTOR=ON -DKITTAI_KEY_WORD_DETECTOR_LIB_PATH=.../snowboy-1.2.0/lib/libsnowboy-detect.a -DKITTAI_KEY_WORD_DETECTOR_INCLUDE_DIR=.../snowboy-1.2.0/include


### Build with MediaPlayer

    cmake <path-to-source> -DGSTREAMER_MEDIA_PLAYER=ON -DCMAKE_PREFIX_PATH=<path-to-GStreamer-build>


### Build with PortAudio

    cmake <path-to-source> -DPORTAUDIO=ON -DPORTAUDIO_LIB_PATH=.../portaudio/lib/.libs/libportaudio.a -DPORTAUDIO_INCLUDE_DIR=.../portaudio/include




## Build for Linux


_tbd_



### Application settings


_tbd_



## Build Doc

    make doc



## Run


### Run AuthServer


### Run unit tests


### Run sample app





## Install


    make install





## References

* [AVS Device SDK](https://developer.amazon.com/alexa-voice-service/sdk)
* [alexa/avs-device-sdk](https://github.com/alexa/avs-device-sdk)
* [AlexaClientSDK Doc](https://alexa.github.io/avs-device-sdk/)
* [Linux Quick Start Guide](https://github.com/alexa/avs-device-sdk/wiki/Linux-Quick-Start-Guide)
* [Raspberry Pi Quick Start Guide](https://github.com/alexa/avs-device-sdk/wiki/Raspberry-Pi-Quick-Start-Guide)



